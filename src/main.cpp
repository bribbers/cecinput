#include "cecinput.h"
#include <QGuiApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    CECInput w;
    if (!w.isOpened()) {
        qWarning() << "Could not open";
        return 1;
    }

    return app.exec();
}

