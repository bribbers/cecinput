#include "cecinput.h"
#include <QDebug>

#include <iostream> //???
#include <unistd.h>
#include <stdio.h>
#include <linux/input.h>
#include <libcec/cec.h>
#include <libcec/cecloader.h>
#include <libcec/cectypes.h>

#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/fakeinput.h>
#include <KWayland/Client/registry.h>

#include "cec_logging.h"

using namespace CEC;

void handleCecKeypress(void* data, const CEC::cec_keypress* key)
{
    CECInput *instance = static_cast<CECInput *>(data);

    static const QHash<int, int> keyCodeTranslation = {
        { CEC::CEC_USER_CONTROL_CODE_PLAY, KEY_PLAY},
        { CEC::CEC_USER_CONTROL_CODE_STOP, KEY_STOP},
        { CEC::CEC_USER_CONTROL_CODE_REWIND, KEY_REWIND},
        { CEC::CEC_USER_CONTROL_CODE_FAST_FORWARD, KEY_FASTFORWARD},
        { CEC::CEC_USER_CONTROL_CODE_SELECT, KEY_SELECT},
        { CEC::CEC_USER_CONTROL_CODE_UP, KEY_UP},
        { CEC::CEC_USER_CONTROL_CODE_DOWN, KEY_DOWN},
        { CEC::CEC_USER_CONTROL_CODE_LEFT, KEY_LEFT},
        { CEC::CEC_USER_CONTROL_CODE_RIGHT, KEY_RIGHT},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER0, KEY_0},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER1, KEY_1},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER2, KEY_2},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER3, KEY_3},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER4, KEY_4},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER5, KEY_5},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER6, KEY_6},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER7, KEY_7},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER8, KEY_8},
        { CEC::CEC_USER_CONTROL_CODE_NUMBER9, KEY_9},
        { CEC::CEC_USER_CONTROL_CODE_F1_BLUE, KEY_BLUE},
        { CEC::CEC_USER_CONTROL_CODE_F2_RED, KEY_RED},
        { CEC::CEC_USER_CONTROL_CODE_F3_GREEN, KEY_GREEN},
        { CEC::CEC_USER_CONTROL_CODE_F4_YELLOW, KEY_YELLOW},
        { CEC::CEC_USER_CONTROL_CODE_CHANNEL_UP, KEY_CHANNELUP},
        { CEC::CEC_USER_CONTROL_CODE_CHANNEL_DOWN, KEY_CHANNELDOWN},
        { CEC::CEC_USER_CONTROL_CODE_EXIT, KEY_EXIT},
        { CEC::CEC_USER_CONTROL_CODE_AN_RETURN, KEY_BACK},
        { CEC::CEC_USER_CONTROL_CODE_ROOT_MENU, KEY_HOME},
        { CEC::CEC_USER_CONTROL_CODE_SUB_PICTURE, KEY_SUBTITLE},
        { CEC::CEC_USER_CONTROL_CODE_DISPLAY_INFORMATION, KEY_INFO},
    };
    int nativeKeyCode = keyCodeTranslation.value(key->keycode, -1);

    if (!instance->m_waylandInput) {
        qDebug() << "skipping event as we have not received the wayland interface yet" << nativeKeyCode;
        return;
    }

    if (nativeKeyCode != -1) {
		if (key->duration) {
		    instance->m_waylandInput->requestKeyboardKeyPress(nativeKeyCode);
			qDebug(KWIN_CEC) << "Key press send!";
		} else {
		    instance->m_waylandInput->requestKeyboardKeyRelease(nativeKeyCode);
			qDebug(KWIN_CEC) << "Key release send!";
		}
    } else {
		qWarning() << "Could not interpret CEC keycode: " << key->keycode;
    }
}

void handleCecLogMessage(void *param, const cec_log_message* message)
{
    Q_UNUSED(param);

    std::string strLevel;
    switch (message->level)
    {
    case CEC_LOG_ERROR:
        qCCritical(KWIN_CEC) << "CRITICAL: " << message->message;
        break;
    case CEC_LOG_WARNING:
        qCWarning(KWIN_CEC) << "WARNING: " << message->message;
        break;
    case CEC_LOG_NOTICE:
    case CEC_LOG_TRAFFIC:
        qCInfo(KWIN_CEC) << "INFO: " << message->message;
        break;
        break;
    case CEC_LOG_DEBUG:
        qCDebug(KWIN_CEC) << "DEBUG: " << message->message;
        break;
    default:
        break;
    }
    std::cout << message->message;

}

void handleCecAlert(void *param, const libcec_alert type, const libcec_parameter cecparam)
{
    Q_UNUSED(param)
    Q_UNUSED(cecparam)
    switch (type)
    {
    case CEC_ALERT_CONNECTION_LOST:
        qCWarning(KWIN_CEC) << "Connection lost!";
        break;
    default:
        break;
    }
}

CECInput::CECInput()
    : QObject()
{
    {
        using namespace KWayland::Client;
        ConnectionThread* connection = ConnectionThread::fromApplication(this);
        if (!connection) {
            qDebug() << "failed to get the Connection from Qt, Wayland remote input will not work";
            return;
        }
        Registry* registry = new Registry(this);
        registry->create(connection);
        connect(registry, &Registry::fakeInputAnnounced, this,
            [this, registry] (quint32 name, quint32 version) {
                m_waylandInput = registry->createFakeInput(name, version, this);
                connect(registry, &Registry::fakeInputRemoved, m_waylandInput, &QObject::deleteLater);
                m_waylandInput->authenticate({}, {});
            }
        );
        registry->setup();
    }

    CEC::ICECCallbacks cec_callbacks;
    cec_callbacks.Clear();
    cec_callbacks.keyPress        = &handleCecKeypress;
    cec_callbacks.logMessage      = &handleCecLogMessage;
    cec_callbacks.alert           = &handleCecAlert;

    CEC::libcec_configuration cec_config;
#ifdef LIBCEC_OSD_NAME_SIZE
    snprintf(cec_config.strDeviceName, LIBCEC_OSD_NAME_SIZE, "KWin CECInput");
#endif
    cec_config.clientVersion = LIBCEC_VERSION_CURRENT;
    cec_config.callbacks = &cec_callbacks;
    cec_config.callbackParam = this;
    cec_config.deviceTypes.Add(CEC::CEC_DEVICE_TYPE_RECORDING_DEVICE);

    m_cecAdapter = LibCecInitialise(&cec_config);

    if(!m_cecAdapter) {
        qDebug() << "Could not create CEC adaptor with current config";
        return;
    }

    CEC::cec_adapter_descriptor device;
    int devices_found = m_cecAdapter->DetectAdapters(&device, 1, NULL);
    if(devices_found < 1) {
        qWarning() << "No CEC devices detected";
        return;
    }

    if(!m_cecAdapter->Open(device.strComName)) {
        qWarning() << "Could not open CEC device" << device.strComPath << device.strComName;
        return;
    }

    m_opened = true;
    qInfo() << "CEC connection opened";

    // Make sure we never quit otherwise the callbacks won't ever be called
    while(true)
    {
        usleep(50000);
    }
}

CECInput::~CECInput() {
    if (m_cecAdapter) {
        if (m_opened)
        {
            m_cecAdapter->Close();
        }
        UnloadLibCec(m_cecAdapter);
        m_cecAdapter = nullptr;
    }
}
